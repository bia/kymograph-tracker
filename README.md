# Kymograph Tracker

<!-- badges: start -->
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Twitter](https://img.shields.io/twitter/follow/Icy_BioImaging?style=social)](https://twitter.com/Icy_BioImaging)
[![Image.sc forum](https://img.shields.io/badge/discourse-forum-brightgreen.svg?style=flat)](https://forum.image.sc/tag/icy)
<!-- badges: end -->

This is the repository for the source code of *Kymograph Tracker*, a plugin for the [bioimage analysis software Icy](http://icy.bioimageanalysis.org/), which was developed by members or former members of the [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). This plugin is licensed under GPL3 license.     
Icy is developed and maintained by [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). The [source code of Icy](https://gitlab.pasteur.fr/bia/icy) is also licensed under a GPL3 license.     



## Plugin description

<!-- Short description of goals of package, with descriptive links to the documentation website --> 

This plugin allows the extraction of the kymograph representation of pixel intensity evolution through time and along a defined spatial path. Different utilities are embedded to define the extraction curve.

Multiple paths can be used for the same image sequence.

Once the kymograph representation is extracted, tracks can be traced as 2D curves in the kymograph images. Those tracks can be save as a sequence of 1D positions along the extraction path through time, or exported to the TrackManager pluging for 2D+T representation and analysis.

Results export and import functionalities are provided.

NB: only 2D+t image sequences can be proecessed and only the first channel is used for the extraction of the kymograph images.

For a reference, please cite:
Chenouard, N., Buisson, J.,  Bloch, I.,  Bastin, P. and Olivo-Marin, J.-C.
"Curvelet analysis of kymograph for tracking bi-directional particles in fluorescence microscopy images", 17th IEEE International Conference on Image Processing (ICIP), 2010        
A more detailed user documentation can be found on the Kymograph Tracker documentation page on the Icy website: http://icy.bioimageanalysis.org/plugin/kymographtracker/               


## Installation instructions

For end-users, refer to the documentation on the Icy website on [how to install an Icy plugin](http://icy.bioimageanalysis.org/tutorial/how-to-install-an-icy-plugin/).      

For developers, see our [Contributing guidelines](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CONTRIBUTING.md) and [Code of Conduct](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CODE-OF-CONDUCT.md).      

<!--  Here we should have some explanations on how to fork this repo (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Add any info related to Maven etc. How the project is build (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Any additional setup required (authentication tokens, etc).  -->


## Main functions and usage

<!-- list main functions, explain architecture, classname, give info on how to get started with the plugin. If applicable, how the package compares to other similar packages and/or how it relates to other packages -->

Classname: `plugins.nchenouard.kymographtracker.KymographTracker`



## Citation 

Please cite this plugins as follows:          


Please also cite the Icy software and mention the version of Icy you used (bottom right corner of the GUI or first lines of the Output tab):     
de Chaumont, F. et al. (2012) Icy: an open bioimage informatics platform for extended reproducible research, [Nature Methods](https://www.nature.com/articles/nmeth.2075), 9, pp. 690-696       
http://icy.bioimageanalysis.org    



## Author(s)      

Nicolas Chenouard


## Additional information






