package plugins.nchenouard.pathtracing;

/**
 * Events describing the changes of path traced with the InteractiveDjikstraTracing painter
 * */

public enum PathEvent
{
	RESET_PATH, TEMPORARY_PATH, FINAL_PATH
}
